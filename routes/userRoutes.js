// LOAD EXPRESS
	const express = require("express");
	const router = express.Router();
	const auth = require("../auth");

//	LOAD userController
const userController = require("../controllers/userController")

/* ADDITIONAL ROUTE FOR GETTING DATA*/ 
	router.get ("/all", (req, res) =>
	{
		userController.getAllUsers().then(resultFromController => res.send(resultFromController));
	})


// ROUTE FOR CHECKING: if the user's email already exists in our database
	router.post("/checkEmail", (req, res) =>
	{
		userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
	});

// ROUTE FOR CREATING: User Registration
	router.post("/register",(req,res) =>
	{
		userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
	});

// ROUTE FOR AUTHENTICATION: User Login
	router.post("/login", (req,res) =>
	{
		userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
	});

// ACTIVITY: ROUTE FOR GETTING DETAILS
	router.post("/details.act", (req, res) =>
	{
		userController.getProfile(req.body).then(resultFromController => res.send(resultFromController));
	});

	//alt solution - updated for s39
	router.post("/details", auth.verify, (req, res) =>
	{
		// Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
		const userData = auth.decode(req.headers.authorization)
		console.log (userData);
			/*
				{
				  id: '634015c111347e2240da7ade',
				  email: 'jane@email.com',
				  isAdmin: false,
				  iat: 1665402615
				}

			*/
		console.log (req.headers.authorization);
		// Provides the user's ID for the getProfile controller method
		userController.getProfile2({id : userData.id}).then(resultFromController => res.send(resultFromController)); 
	});


/*		--------------s41----------------		*/

// ROUTE TO ENROLL USER TO A COURSE
	router.post("/enroll" , auth.verify, (req,res) =>
	{
		let data =
		{
			userId: auth.decode(req.headers.authorization).id,
			courseId: req.body.courseId,
			isAdmin: auth.decode(req.headers.authorization).isAdmin
		}
		userController.enroll(data).then(resultFromController => res.send(resultFromController));
	});



// EXPORT MODULES
	module.exports = router;