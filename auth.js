const jwt = require("jsonwebtoken");

const secret = "CourseBookingAPI";

// JSON Web Tokens

	// Token Creation
	module.exports.createAccessToken = (user) =>
	{
		//console.log(user)
		/*
			{
			  _id: new ObjectId("634015c111347e2240da7ade"),
			  firstName: 'Jane',
			  lastName: 'Doe',
			  email: 'jane@email.com',
			  password: '$2b$10$gYfVSKOIu.5KM1bgV6et7ONqjIaBPwOWNT95CJw68MLpiUssaqEEi',
			  isAdmin: false,
			  mobileNo: '09666666666',
			  enrollments: [],
			  __v: 0
			}

		*/
		// Modify the payload
		const data =
		{
			id: user._id,
			email: user.email,
			isAdmin: user.isAdmin
		}

		// Generate a token 
			/*
				jwt.sign 
					(<payload>, 
					<CourseBookingAPI>,
					option - e.g. {expiresIn: "2h"} )
			*/
		return jwt.sign(data, secret, {})
	};


	//Token Verification
	module.exports.verify = (req, res, next) => 
	{
		let token = req.headers.authorization

		if (typeof token !== "undefined" )
		{
			//console.log (token)
			/*
				7 characters (Bearer<space>))
				Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYzNDAxNWMxMTEzNDdlMjI0MGRhN2FkZSIsImVtYWlsIjoiamFuZUBlbWFpbC5jb20iLCJpc0FkbWluIjpmY
				WxzZSwiaWF0IjoxNjY1NDAyNjE1fQ.eAntMh-0heeio01lmRwAhs1kG1WAz0FUcLEvy3cc34I
			*/
			token = token.slice(7, token.length);
			
			//console.log (token)
			/*
				eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYzNDAxNWMxMTEzNDdlMjI0MGRhN2FkZSIsImVtYWlsIjoiamFuZUBlbWFpbC5jb20iLCJpc0FkbWluIjpmYWxzZSwiaWF0IjoxNjY1NDAyNjE1fQ.eAntMh-0heeio01lmRwAhs1kG1WAz0FUcLEvy3cc34I
			*/

			return jwt.verify(token, secret, (err,data) =>
			{
				if (err)
				{
					return res.send({auth:"failed"})
				}
				else 
				{	next()}
			})
		}

		else 
		{
			return res.send({auth: "failed"});
		}
	};


	// Token Decryption
	module.exports.decode = (token) =>
	{
		if (typeof token !== "undefined")
		{
			token = token.slice(7, token.length);

			return jwt.verify (token, secret, (err,data) =>
			{
				if (err)
				{	return null	}
				else 
				{
					return jwt.decode(token, {complete:true}).payload
				}
			})
		}
		else
		{
			return null
		}
	};


