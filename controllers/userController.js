// CONNECT TO MODEL CONTENTS & FILES
	const User = require("../models/User");
	const Course = require("../models/Course");
	const bcrypt = require("bcrypt");
	const auth = require("../auth");


// Controller fo Getting all Users
	module.exports.getAllUsers =() =>
	{
		return User.find({}).then (result =>
		{
			return result
		})
	}

// Controller for Checking if email exists
	module.exports.checkEmailExists = (reqBody) =>
	{
		return User.find({email: reqBody.email}) .then(result =>
		{
			if (result.length > 0)
				{ return true }
			else
				{ return false }
		})
	}

// Controller for User Registration
	module.exports.registerUser = (reqBody) =>
	{
		let newUser = new User (
		{
			firstName: reqBody.firstName,
			lastName: reqBody.lastName,
			email: reqBody.email,
			mobileNo: reqBody.mobileNo,
			// bcrypt.hashSynch(<dataToBeHashed>, <saltRound>)
			password: bcrypt.hashSync(reqBody.password,10)
		})

		return newUser.save().then((user,error) =>
		{
			if (error)
				{return false}
			else
				{return true}
		})
	}

// Controller for Login
	module.exports.loginUser = (reqBody) =>
	{
		return User.findOne({email: reqBody.email}).then (result =>
		{
			if (result == null)
				{ return false	}
			else
			{
				const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
				//bcrypt.compareSync(<dataTobeCompared>, <datafromDB>)

					if (isPasswordCorrect)
					{	console.log(result)
						return {access: auth.createAccessToken(result)}
					}
					else
					{	return false }
			}
		})
	}

// ACTIVITY: Controller for Getting Details
	module.exports.getProfile = (reqBody) =>
	{
		return User.findOne({_id: reqBody._id}).then((result, error) =>
		{	
			if (result == null)
				{ return false	}

			if (result !== null )
				{
					result.password = ""
					return result
				}
				/*{ return {	_id: result._id,
							firstName: result.firstName,
							lastName: result.lastName,
							email: result.email,
							password: "" ,
							isAdmin: result.isAdmin,
							mobileNo: result.mobileNo,
							enrollments: result.enrollments, 
							__v: result.__v	} 
				}*/
		})
	} 

// alternate solution
	module.exports.getProfile2 = (data) =>
	{
		return User.findById(data.id).then(result =>
		{
			console.log (result)
			result.password = "secret";
			return result
		});
	}

/*		--------------s41----------------		*/

// Controller to Enroll a user to a class
	module.exports.enroll = async (data) =>
	{
		if (data.isAdmin === false)
		{
			let isUserUpdated = await User.findById(data.userId).then(user =>
			{
				user.enrollments.push({courseId: data.courseId})
				return user.save().then((user,error) =>
				{
					if(error)
						{	return false}
					else
						{	return true}
				})

			});

			let isCourseUpdated = await Course.findById(data.courseId).then(course =>
			{
				course.enrollees.push({userId: data.userId})
				return course.save().then((course,error) =>
				{
					if(error)
						{	return false}
					else
						{	return true}
				})

			});
			
			if (isUserUpdated && isCourseUpdated)
				{	return true}
			else 
				{	return false}
		}

		else
		{
			return "Must be Student to enroll to a course"
		}


	}
