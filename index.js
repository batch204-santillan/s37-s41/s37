
// DEPENDENCIES
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// IMPORT userRoutes
const userRoutes = require("./routes/userRoutes")
// IMPORT courseRoutes
const courseRoutes = require("./routes/courseRoutes")

// SERVER SETUP
const port = process.env.PORT || 4000;
const app = express();

//DATABASE CONNECTION (MongoDB)
mongoose.connect("mongodb+srv://rafsantillan:admin123@batch204-santillan.1nyzknv.mongodb.net/s37-s41?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

	let db = mongoose.connection;
	db.on("error", () => console.error.bind(console,"Error"));
	db.once("open", () => console.log ("Now Connected to MongoDB Atlas!"));

//MIDDLEWARES
app.use(express.json());
app.use(cors());

//ADD the userRoutes 
app.use("/users", userRoutes);
	/*localhost:4000/users/ENDPOINT*/
// ADD the courseRoutes
app.use("/courses", courseRoutes);

app.listen(port, () => 
	{
		console.log (`API now online in port ${port}.`)
	});