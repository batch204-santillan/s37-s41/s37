// CONNECT TO FILES & MODELS
	const Course = require("../models/Course")
	const User = require("../models/User")


// Controller for Creating New Course
	module.exports.addCourse = (reqBody) => 
	{
		let newCourse = new Course (
		{
			name: reqBody.name ,
			description: reqBody.description ,
			price: reqBody.price
		});

		return newCourse.save().then((course,error) =>
		{
			if (error)
				{	return false}
			else
				{	return true}
		});
	}


// Controller for Retrieving all Courses
	module.exports.getAllCourses =() =>
	{
		return Course.find({}).then (result =>
		{
			return result;
		})
	}

// Controller for retrieving all active courses
	module.exports.getAllActive = () =>
	{
		return Course.find({isActive: true}).then(result =>
		{
			return result;
		})
	}

// Controller for retrieving a specific course
	module.exports.getCourse=(reqParams) =>
	{
		return Course.findById(reqParams.courseId).then(result=>
		{
			return result;
		})
	}

// Controller for Updating a Course
	module.exports.updateCourse=(reqParams, reqBody, data) =>
	{
		return User.findById(data.id).then(result =>
		{
			console.log (result)
			if (result.isAdmin === true)
			{
				let updatedCourse = 
				{
					name: reqBody.name,
					description: reqBody.description,
					price: reqBody.price
				};
				return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) =>
				{
					if (error)
					{	return false}
					else 
					{	return true}
				})
			}
			else
			{	return false}
		})
	}
	
// ACTIVITY ARCHIVE A COURSE!

	// Controller for ARCHIVING
	module.exports.archiveCourse=(reqParams, reqBody, data) =>
	{
		if (data.isAdmin)
		{
			// using this, will no longer require to input data on the request body
			let archivedCourse = 
			{
				isActive: false
			};


			return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course,error) =>
			{
				if (error)
				{	return false}
				else 
				{	
					return "Course Successfully Archived!" 
					//return course 
				}
			})

			
		}
		else
		{	return false}
	}

	// Controller for ACTIVATING
	module.exports.activateCourse= (reqParams, reqBody, data) =>
	{
		if (data.isAdmin)
		{
			let activatedCourse = 
			{
				isActive : true
			};
		return Course.findByIdAndUpdate(reqParams.courseId, activatedCourse).then((course, error) =>
			{
				if (error)
				{	return false}
				else 
				{	
					return "Course is now Active!" 
					//return course 
				}
			})
		}
		else
		{	return false}
	}