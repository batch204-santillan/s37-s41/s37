// LOAD EXPRESS 
	const express = require("express");
	const router = express.Router();
	const courseController = require("../controllers/courseController");
	const auth = require("../auth.js")

// ROUTE FOR CREATING A COURSE
	router.post("/", auth.verify, (req,res) =>
	{
		// Decode the user's Data
		const isAdmin = auth.decode(req.headers.authorization).isAdmin;
		console.log (isAdmin);

		if (isAdmin)
		{
			courseController.addCourse(req.body).then(resultFromController=> res.send(resultFromController));
		}
		else 
		{	res.send( "You have to be ADMIN to create a course!")}
	});


// ROUTE FOR RETRIEVING ALL COURSES
	router.get ("/all", (req, res) =>
	{
		courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
	});


// ROUTE FOR RETRIEVING ALL ACTIVE COURSES
	router.get ("/" , (req, res) =>
	{
		courseController.getAllActive().then(resultFromController => res.send(resultFromController));
	});

// ROUTE FOR RETRIEVING A SPECIFIC COURSE
	router.get("/:courseId", (req,res) =>
	{
		console.log (req.params.courseId);
		courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
	});

// ROUTE FOR UPDATING A COURSE
	router.put ("/:courseId", auth.verify, (req, res) =>
	{
		const userData = auth.decode (req.headers.authorization)

		courseController.updateCourse(req.params, req.body , userData).then(resultFromController => res.send(resultFromController));
	});


// ACTIVITY ARCHIVE A COURSE!
	
	//ARCHIVING
	router.put("/:courseId/archive", auth.verify, (req,res) =>
	{
		const userData = auth.decode(req.headers.authorization)

		courseController.archiveCourse(req.params, req.body, userData).then(resultFromController => res.send(resultFromController));
	});

	//ACTIVATING
	router.put("/:courseId/activate", auth.verify, (req,res) =>
	{
		const userData = auth.decode(req.headers.authorization)

		courseController.activateCourse(req.params, req.body, userData).then(resultFromController => res.send(resultFromController));
	});



// EXPORT MODULES
module.exports = router;

/*
	Mini Activity:
		Limit course creation to admin only.
		► Refactor the course route/controller.
*/